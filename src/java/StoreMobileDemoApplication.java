package com.phuhtv.StoreMobileDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class StoreMobileDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(StoreMobileDemoApplication.class, args);
	}

}
