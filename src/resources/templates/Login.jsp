<%-- 
    Document   : Login
    Created on : May 31, 2022, 7:16:26 PM
    Author     : tsuna
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Form</title>
        <link rel="stylesheet" href="/css/Login.css" />
        <link
            href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"
            rel="stylesheet"
            id="bootstrap-css"
            />
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    </head>
    <style>
        * {
            margin: 0;
            padding: 0;
        }
        .all_page_login {
            width: 100%;
            height: auto;
        }
        .head_page_login {
            height: 120px;
            font-size: 40px;
            line-height: 120px;
            margin: 0 0 40px 0;
            background-color: rgb(222, 222, 222);
        }
        .head_title {
            width: 90%;
            margin: 0 5%;
            font-family: "Lucida Sans", "Lucida Sans Regular", "Lucida Grande",
                "Lucida Sans Unicode", Geneva, Verdana, sans-serif;
        }

    </style>
    <body>
        <div class="all_page_login">
            <div class="head_page_login">
                <div class="head_title">Mobile Store</div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Please sign in</h3>
                        </div>
                        <div class="panel-body">
                            <form accept-charset="UTF-8" role="form">
                                <fieldset>
                                    <div class="form-group">
                                        <input
                                            class="form-control"
                                            placeholder="Username"
                                            name="Username"
                                            type="text"
                                            />
                                    </div>
                                    <div class="form-group">
                                        <input
                                            class="form-control"
                                            placeholder="Password"
                                            name="password"
                                            type="password"
                                            value=""
                                            />
                                    </div>

                                    <input
                                        class="btn btn-lg btn-success btn-block"
                                        type="submit"
                                        value="Login"
                                        />
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

